# Introducción a Numpy

En este labo repasamos como manejarnos con vectores y matrices en Python, utilizando Numpy.
Aparte, programamos algunos ejercicios de la guía 1, y una función para escalonar matrices.