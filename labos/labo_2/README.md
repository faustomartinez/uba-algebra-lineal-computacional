# Numéros de máquina

En este laboratorio, vemos con detalle en Python numerosos problemas que pueden surgir a raíz de guardar los números con precisión simple (y que ocurren igual al usar precisión doble).

Se aborda desde la aritmética del punto flotante hasta los límites de representación de números de máquina.