# Álgebra Lineal Computacional

2do cuatrimestre 2024 \
Universidad de Buenos Aires

## Material teórico

[Apunte de Santiago Laplagne y Gabriel Acosta](https://github.com/slap/ALC-apunte)

## Prácticas

| Nro | Enunciado                                                                                          | Solución                                                                                                      |
|-----|----------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------|
| 1   | [Enunciado](https://gitlab.com/faustomartinez/uba-algebra-lineal-computacional/-/blob/main/practicas/enunciados/practica1.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-algebra-lineal-computacional/-/blob/main/practicas/soluciones/practica1.ipynb)
| 2   | [Enunciado](https://gitlab.com/faustomartinez/uba-algebra-lineal-computacional/-/blob/main/practicas/enunciados/practica2.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-algebra-lineal-computacional/-/blob/main/practicas/soluciones/practica2.ipynb)
| 3   | [Enunciado](https://gitlab.com/faustomartinez/uba-algebra-lineal-computacional/-/blob/main/practicas/enunciados/practica3.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-algebra-lineal-computacional/-/blob/main/practicas/soluciones/practica3.ipynb)
| 4   | [Enunciado](https://gitlab.com/faustomartinez/uba-algebra-lineal-computacional/-/blob/main/practicas/enunciados/practica4.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-algebra-lineal-computacional/-/blob/main/practicas/soluciones/practica4.ipynb)
| 5   | [Enunciado](https://gitlab.com/faustomartinez/uba-algebra-lineal-computacional/-/blob/main/practicas/enunciados/practica5.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-algebra-lineal-computacional/-/blob/main/practicas/soluciones/practica5.ipynb)
| 6   | [Enunciado](https://gitlab.com/faustomartinez/uba-algebra-lineal-computacional/-/blob/main/practicas/enunciados/practica6.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-algebra-lineal-computacional/-/blob/main/practicas/soluciones/practica6.ipynb)
| 7   | [Enunciado](https://gitlab.com/faustomartinez/uba-algebra-lineal-computacional/-/blob/main/practicas/enunciados/practica7.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-algebra-lineal-computacional/-/blob/main/practicas/soluciones/practica7.ipynb)


## Laboratorios
| Nro | Título                                                                                          | Carpeta                                                                                                      |
|-----|----------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------|
| 1   | Introducción | [Carpeta](https://gitlab.com/faustomartinez/uba-algebra-lineal-computacional/-/blob/main/labos/labo_1)
| 2   | Números de máquina | [Carpeta](https://gitlab.com/faustomartinez/uba-algebra-lineal-computacional/-/blob/main/labos/labo_2)
| 3   | Transformaciones lineales | [Carpeta](https://gitlab.com/faustomartinez/uba-algebra-lineal-computacional/-/blob/main/labos/labo_3)
| 4   | Número de condición | [Carpeta](https://gitlab.com/faustomartinez/uba-algebra-lineal-computacional/-/blob/main/labos/labo_4)
| 5   | Descomposición LU | [Carpeta](https://gitlab.com/faustomartinez/uba-algebra-lineal-computacional/-/blob/main/labos/labo_5)
